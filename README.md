# Elektronické úlohy pre ChatGPT

1. Prepisovanie schém

    Chceme zistiť, či ChatGPT správne chápe schémy elektrických obvodov. Zaujíma nás, či vie popísať schému, ktorá je mu zadaná obrázkom, alebo textom.

    - [Trojboký ihlan](./prepis-schem/trojboky-ihlan/README.md)

2. Riešenie úloh

   Chceme zistiť, ako sa bude dariť ChatGPT riešiť elektronické úlohy. Zaujíma nás, či vie správne postupovať pri riešení schém, aký postup zvolí a či sa dopracuje k správnemu výsledku.

    - [Trojboký ihlan](./riesenie-schem/trojboky-ihlan/README.md)
