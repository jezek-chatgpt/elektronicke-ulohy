# Elektronické úlohy pre ChatGPT [🔗](../../README.md)

## Riešenie schém

ChatGPT je zadaná úloha z elektroniky a žiadosť o vyrišenie tejto úlohy.

### Trojboký ihlan

Keďže sme zistili, že ChatGPT 4 V nevie spoľahlivo interpretovať obrázok so schémou, ale zo zadania mu to ide ([tu](../../prepis-schem/trojboky-ihlan/README.md)), tak sa nebudeme zaťažovať zadávaním úlohy vo forme obrázku (či textu a obrázku) a budeme používať len textové zadanie.

```
Šesť rezistorov o hodnote R=1kΩ tvoria hrany trojbokého ihlanu. Vypočítajte výsledný odpor medzi dvomi vrcholmi ihlanu.
```

- ChatGPT 4 V
    - [Anglická verzia dotazu](./text-only/english-prompt_no-custom-instructions.md) - Úspešnosť 50%
    - [Slovenská verzia dotazu](./text-only/slovak-prompt_no-custom-instructions.md) - Úspešnosť 0%

- ChatGPT 4 V - Circuit Sage
    - [Anglická verzia dotazu](./text-only/english-prompt_circuit-sage.md) - Úspešnosť 20%
    - [Slovenská verzia dotazu](./text-only/slovak-prompt_circuit-sage.md) - Úspešnosť 0%


### Riešenie
   - [Matematicky](./riesenie-mat.pdf)
   - [Simulačne](./riesenie-simul.pdf)
