# Elektronické úlohy pre ChatGPT [🔗](../../README.md)

## Prepisovanie schém

Aby sa nám ľahšie kontrolovalo, či správne pochopil schému, necháme ho zapísať schému pomocou netlistu. Netlist je textový popis elektronického obvodu, ktorý obsahuje zoznam komponentov a informácie o ich vzájomných prepojeniach. Netlistov je viac typov v našom prípade použijeme SPICE netlist. SPICE netlist sa používa pre simulácie obvodov v programe SPICE (Simulation Program with Integrated Circuit Emphasis). Obsahuje detailný popis komponentov a ich prepojení pre analýzu obvodu. SPICE netlist je ževraj najznámejší z netlistov, preto ho budeme používať pre naše testy.

ChatGPT je zadaný obrázok, alebo zadanie úlohy a žiadosť o prepis schémy do SPICE netlist-u.

### Trojboký ihlan

1. Z obrázku

    ![Schéma trojbokého ihlanu s odpormi](./picture-with-text/schema.png)

    (Poznámka: Treba si dať pozor na názov obrázku, môže z neho vyčítať informácie.)

    - ChatGPT 4 V
        - [Anglická verzia dotazu](./picture-with-text/english-prompt_no-custom-instructions.md) - Úspešnosť 30%
        - [Slovenská verzia dotazu](./picture-with-text/slovak-prompt_no-custom-instructions.md) - Úspešnosť 40%
    - ChatGPT 4 V - Circuit Sage
        - [Anglická verzia dotazu](./picture-with-text/english-prompt_circuit-sage.md) - Úspešnosť 55%
        - [Slovenská verzia dotazu](./picture-with-text/slovak-prompt_circuit-sage.md) - Úspešnosť 40%
2. Zo zadania

    ```
    Šesť rezistorov o hodnote R=1kΩ tvoria hrany trojbokého ihlanu. Vypočítajte výsledný odpor medzi dvomi vrcholmi ihlanu.
    ```

    - ChatGPT 4 V
        - [Anglická verzia dotazu](./text-only/english-prompt_no-custom-instructions.md) - Úspešnosť 100%
        - [Slovenská verzia dotazu](./text-only/slovak-prompt_no-custom-instructions.md) - Úspešnosť 90%
